import React, { Component } from "react";
import { QueryRenderer, ReadyState } from "react-relay";
import environment from "./env";
import appQuery from "./gql/queries/app";

import Films from "./films";

class App extends Component {
  render() {
    return (
      <QueryRenderer
        environment={environment}
        query={appQuery}
        variables={{ count: 3 }}
        render={({ error, props }: ReadyState<any>) => {
          if (error) {
            return <div>`Error: ${error.message}`</div>;
          } else if (props) {
            return <Films films={props} />;
          }

          return <div>Loading...</div>;
        }}
      />
    );
  }
}

export default App;
