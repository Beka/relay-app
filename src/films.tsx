import React, { PureComponent, Fragment } from "react";
import {
  graphql,
  createPaginationContainer,
  RelayPaginationProp
} from "react-relay";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import { films_films } from "./__generated__/films_films.graphql";

interface CommonProps {
  films: films_films;
  relay: RelayPaginationProp;
}

export type FilmsType = CommonProps;

export class Films extends PureComponent<FilmsType> {
  constructor(props: CommonProps) {
    super(props);

    this.mapData = this.mapData.bind(this);
  }

  mapData = () => {
    const { allFilms } = this.props.films;

    console.log("Data inside component: ", allFilms);

    if (allFilms && allFilms.edges && allFilms.edges.length > 0) {
      return allFilms.edges.map(item => ({
        id: item!.cursor,
        cursor: item!.cursor,
        title: item!.node!.title,
        director: item!.node!.director
      }));
    }
  };

  render() {
    const data = this.mapData();

    return (
      <Fragment>
        <Typography variant="h3" gutterBottom>
          StarWars movies:
        </Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Cursor</TableCell>
                <TableCell>Title</TableCell>
                <TableCell>Director</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data &&
                data.map(item => (
                  <TableRow key={item.id}>
                    <TableCell>{item.cursor}</TableCell>
                    <TableCell>{item.title}</TableCell>
                    <TableCell>{item.director}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Paper>
        {this.props.relay.hasMore && (
          <Grid container justify="center">
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                this.props.relay.loadMore(3, error => {
                  console.log(error);
                });
              }}
            >
              Load more...
            </Button>
          </Grid>
        )}
      </Fragment>
    );
  }
}

export default createPaginationContainer(
  Films,
  {
    films: graphql`
      fragment films_films on Root
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 3 }
          after: { type: "String" }
        ) {
        allFilms(first: $count, after: $after)
          @connection(key: "films_allFilms") {
          edges {
            cursor
            node {
              title
              director
            }
          }
          pageInfo {
            hasNextPage
            endCursor
          }
        }
      }
    `
  },
  {
    direction: "forward",
    getConnectionFromProps(props): any {
      return props.films;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, paginationInfo, fragmentVariables) {
      console.log(props);
      return {
        after: paginationInfo.cursor,
        count: paginationInfo.count
      };
    },
    query: graphql`
      query filmsListForwardQuery($count: Int!, $after: String) {
        films: node {
          ...films_films @arguments(count: $count, after: $after)
        }
      }
    `
  }
);
