import { graphql } from "react-relay";

export default graphql`
  query appQuery($count: Int!, $after: String) {
    ...films_films
  }
`;
