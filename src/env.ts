const { Environment, Network, RecordSource, Store } = require("relay-runtime");

const store = new Store(new RecordSource());

const network = Network.create((operation: any, variables: any) => {
  return fetch("http://localhost:56918/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      query: operation.text,
      variables
    })
  }).then(response => response.json());
});

const environment = new Environment({
  network,
  store
});

export default environment;
